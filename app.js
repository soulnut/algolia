import { initAutocomplete } from './autocomplete';
import { initInstantSearch } from './instantsearch';

if (document.getElementById('autocomplete')) { 
    initAutocomplete();
}

if (document.getElementById('ais-InstantSearch')) { 
    initInstantSearch();
}