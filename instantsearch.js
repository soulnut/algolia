
import searchClient from './searchclient';
import instantsearch from 'instantsearch.js';

import { hits } from 'instantsearch.js/es/widgets'
import { connectSearchBox } from 'instantsearch.js/es/connectors';


const urlParams = new URLSearchParams(window.location.search);
const myParam = urlParams.get('search');

const search = instantsearch({
  indexName: 'instant_search',
  searchClient: searchClient,
  //routing: true,
  initialUiState: {
    instant_search: {
        query: myParam
    }
}
});

export const initInstantSearch = () => {
    const virtualSearchBox = connectSearchBox(() => null);

    search.addWidgets([
      hits({
        container: '#hits',
        templates: {
          item: `
            <div>
              <img src="{{image}}" align="left" alt="{{name}}" />
              <div class="hit-name">
                {{#helpers.highlight}}{ "attribute": "name" }{{/helpers.highlight}}
              </div>
              <div class="hit-description">
                {{#helpers.highlight}}{ "attribute": "description" }{{/helpers.highlight}}
              </div>
              <div class="hit-price">\${{price}}</div>
            </div>
          `,
        },
      }),
      virtualSearchBox({
        container: '#searchbox'
      })
    ]);

    search.start();
}