/** @jsx h */
import {
    autocomplete,
    getAlgoliaHits,
    snippetHit,
  } from '@algolia/autocomplete-js';
  import { h, Fragment } from 'preact';

  import searchClient from './searchclient';
  
  import '@algolia/autocomplete-theme-classic';

  
  export const initAutocomplete = () => autocomplete({
    container: '#autocomplete',
    placeholder: 'Search for products',
    getSources({ query }) {
      return [
        {
          sourceId: 'products',
          getItems() {
            return getAlgoliaHits({
              searchClient,
              queries: [
                {
                  indexName: 'instant_search',
                  query,
                  params: {
                    hitsPerPage: 5,
                    attributesToSnippet: ['name:10', 'description:35'],
                    snippetEllipsisText: '…',
                  },
                },
              ],
            });
          },
          templates: {
            item({ item }) {
              return <ProductItem hit={item} />;
            },
          },
        },
        {
            sourceId: 'categories',
            getItems() {
              return getAlgoliaHits({
                searchClient,
                queries: [
                  {
                    indexName: 'instant_search',
                    query,
                    params: {
                      hitsPerPage: 5,
                      attributesToSnippet: ['name:10', 'description:35'],
                      snippetEllipsisText: '…',
                    },
                  },
                ],
              });
            },
            templates: {
              item({ item }) {
                return <ProductItem hit={item} />;
              },
            },
          },

      ];
    },
  });
  
  function ProductItem({ hit }) {
    return (
      <Fragment>
        <div className="aa-ItemIcon aa-ItemIcon--align-top">
          <img src={hit.image} alt={hit.name} width="40" height="40" />
        </div>
        <div className="aa-ItemContent">
          <div className="aa-ItemContentTitle">
            {snippetHit({ hit, attribute: 'name' })}
          </div>
        </div>
      </Fragment>
    );
  }